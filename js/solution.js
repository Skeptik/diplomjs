addEventListener('load', inicializacion, false);

const url = 'https://neto-api.herokuapp.com/pic';
const img = document.querySelector('.current-image');
const menu = document.querySelector('.menu');
const burger = document.querySelector('.burger');
const comments = document.querySelector('.comments');
const draw = document.querySelector('.draw');
const share = document.querySelector('.share');
const board = document.querySelector('.wrap');
const newFile = document.querySelector('.new');
const minX = board.offsetLeft;
const minY = board.offsetTop;
const arrMenu = [comments, draw, share];
let maxX, maxY, chckComm, id;
let coordsX, coordsY;
var movedPiece = null;
var activeMenu = null;

function inicializacion(){
	img.style.display = 'none';
	menu.setAttribute('data-state', 'initial');
	menu.draggable = 'true';
	document.addEventListener('mousedown', drStart, false);
	document.addEventListener('mousemove', drop, false);
	document.addEventListener('mouseup', drag, false);
	burger.style.display = 'none';
	menu.addEventListener('click', clickMenu, false);
	board.addEventListener('drop', onFilesDrop);
	board.addEventListener('dragover', e => e.preventDefault());
	var objInputFile = document.createElement('input');
	objInputFile.type = 'file';
	objInputFile.style.position = 'absolute';
	objInputFile.style.visibility = 'hidden';
	objInputFile.addEventListener('change', changeFile);
	newFile.appendChild(objInputFile);
    document.querySelector('.comments__form').remove();
}

function changeFile(e){
	var files = Array.from(e.target.files);
	addImage(files);
}

function addImage(files){
	files.forEach( data =>{
		if(data.type == 'image/jpeg' || data.type == 'image/png'){
			document.querySelector('.error').style.display = 'none';
            const name = data.name.split('.');
			var formData = new FormData();
			formData.append('title', name[0]);
			formData.append('image', files[0]);
			var promise = fetch(url,{
            body : formData,
            credentials: 'omit',
            method: 'POST',
            })
            .then((res)=>{
                if(200<=res.status && res.status<300){
                    return res;
                }
                throw new Error(response.statusText);
            })
            .then((res) => res.json())
            .then((data) =>{
                id = data.id;
                menu.setAttribute('data-state', 'default');
                burger.style.display = 'inline-block';
                img.style.display = 'block';
                img.src = data.url;
                img.addEventListener('load', e =>{
                	URL.revokeObjectURL(e.target.src);
                });
                img.addEventListener('click', clickImg, false);
            })
            .catch((error)=>{
                console.log('Ошибка ' + error);
            });
		}else{
			document.querySelector('.error').style.display = 'block';
		}
	});
}

function clickSubmit(form){
    var text = document.querySelector('form > .comments__body > textarea').value;
    var urlencoded = 'message=' + encodeURIComponent(text) + '&left=' + coordsX + '&top=' + coordsY;
    var promise = fetch(url+'/'+id+'/comments',{
    body : urlencoded,
    credentials: 'omit',
    method: 'POST',
    headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    .then((res)=>{
        if(200<=res.status && res.status<300){
            return res;
        }
        throw new Error(response.statusText);
    })
    .then((res) => res.json())
    .then((data) =>{
        console.log(data);
    })
    .catch((error)=>{
        console.log('Ошибка ' + error);
    });
    return false;
}

function clickImg(e){
    if(chckComm){
        if(checkComments()){
            var rect = img.getBoundingClientRect();
            coordsX = e.pageX - rect.left;
            coordsY = e.pageY - rect.top;
            var form = document.createElement('form');
            form.method = 'POST';
            form.style.left = coordsX + rect.left  + 'px';
            form.style.top = coordsY + rect.top + 'px';
            form.setAttribute('onsubmit', "return clickSubmit(this)");
            form.setAttribute('class', 'comments__form');
            var span = document.createElement('span');
            span.setAttribute('class', 'comments__marker');
            var input = document.createElement('input');
            input.type = 'checkbox';
            input.setAttribute('class', 'comments__marker-checkbox');
            var div = document.createElement('div');
            div.setAttribute('class', 'comments__body');
            var textarea = document.createElement('textarea');
            textarea.setAttribute('class', 'comments__input');
            textarea.placeholder = 'Напишите ответ...';
            var input_1 = document.createElement('input');
            input_1.type = 'button';
            input_1.value = 'Закрыть';
            input_1.setAttribute('class', 'comments__close');
            var input_2 = document.createElement('input');
            input_2.type = 'submit';
            input_2.value = 'Отправить';
            input_2.setAttribute('class', 'comments__submit');
            div.appendChild(textarea);
            div.appendChild(input_1);
            div.appendChild(input_2);
            form.appendChild(span);
            form.appendChild(input);
            form.appendChild(div);
            board.appendChild(form);
        }
    }
}


function checkComments(){
    const ratio = document.querySelectorAll('.menu__toggle');
    for(let i=0; i<=ratio.length-1; i++){
        if(ratio[i].checked && ratio[i].value == 'on'){
            return true;
        }else{
            return false;
        }
    }
}

function onFilesDrop(e){
	e.preventDefault();
	const files = Array.from(e.dataTransfer.files);
	addImage(files);
}

function clickMenu(e){
	if(e.target.classList.contains('new') || e.target.parentElement.classList.contains('new')){
		const file = document.querySelector('.new > input');
		file.click();
	}
	if(e.target.classList.contains('burger') || e.target.parentElement.classList.contains('burger')){
		for(let i=0; i<=arrMenu.length-1; i++){
            arrMenu[i].style.display = 'inline-block';
            arrMenu[i].setAttribute('data-state', '');
        }
        chckComm = false;
	}
	if(e.target.classList.contains('comments') || e.target.parentElement.classList.contains('comments')){
		activeMenu = comments;
        chckComm = true;
		checkAtrib('comments');
	}
	if(e.target.classList.contains('draw') || e.target.parentElement.classList.contains('draw')){
		activeMenu = draw;
		checkAtrib('draw');
	}
	if(e.target.classList.contains('share') || e.target.parentElement.classList.contains('share')){
		activeMenu = share;
		checkAtrib('share');
	}
}

function checkAtrib(el){
    for(let i=0; i<=arrMenu.length-1; i++){
        if(arrMenu[i].classList.contains(el)){
            arrMenu[i].setAttribute('data-state', 'selected');
        }else{
            arrMenu[i].style.display = 'none';
        }
    }
}

function drag(){
	movedPiece = null;
}

function drop(e){
	e.preventDefault();
	if(movedPiece){
		let x = e.pageX;
		let y = e.pageY;
		x = Math.min(x, maxX);
		y = Math.min(y, maxY);
		x = Math.max(x, minX);
		y = Math.max(y, minY);
		movedPiece.parentElement.style.left = x + 'px';
		movedPiece.parentElement.style.top = y + 'px';
	}
}

function drStart(e){
	if(e.target.classList.contains('drag')){
		movedPiece = e.target;
		var style =getComputedStyle(movedPiece.parentElement);
		maxX = minX + board.offsetWidth - movedPiece.parentElement.offsetWidth - parseFloat(style.border);
		maxY = minY + board.offsetHeight - movedPiece.parentElement.offsetHeight;
	}
}